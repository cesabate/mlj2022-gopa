import sys

sys.path.append("../code/")

import plots as gplt
import noisecalibration as nc
from mpmath import mp
from matplotlib import pyplot as plt

plt.rcParams.update({'font.size': 15})

n=10000
eps=0.1 
delta=1/(n**2)


# ~ gplt.plotTotalVarianceOverRho(n, eps, delta,kappa, False, True)
# ~ gplt.plotTotalVarianceOverRho(n, 1, delta,kappa, False,  True)
# ~ gplt.plotTotalVarianceOverRho(n, 0.01, delta,kappa, False, True)
# ~ gplt.plotTotalVarianceOverRho(n, eps, mp.fdiv(1,10**n),kappa, False, True)
# ~ gplt.plotTotalVarianceOverRho(n, 0.001, delta,kappa ,False, True)

kappav = [0.2, 0.4, 1, 10, 100]

gplt.plotTotalVarianceOverRhoKappa(n, 0.1, delta, kappav, nc.GTYPE_KOUT)

# ~ gplt.plotTotalVarianceOverRhoKappa(n, 1, delta,kappav, nc.GTYPE_KOUT)
# ~ gplt.plotTotalVarianceOverRhoKappa(n, 0.1, mp.fdiv(1,10**n), kappav, nc.GTYPE_KOUT)
# ~ gplt.plotTotalVarianceOverRhoKappa(n, 0.01, delta, kappav, nc.GTYPE_KOUT)
# ~ gplt.plotTotalVarianceOverRhoKappa(n, 0.001, delta, kappav, nc.GTYPE_KOUT)


# ~ gplt.plotTotalVarianceOverRho(n, 0.1, delta, 10, True, True)
