# run with: python gen.py

import sys

sys.path.append("../code/")

import sgd 
import noisecalibration as nc 
import numpy as np
from matplotlib import pyplot as plt 
import pickle 
import os 


plt.rcParams.update({'font.size': 15})


"""
gamma_m[i] is the  value of gamma for tuned (i+1)*10 iterations of method m
"""
n_iter_l = [ 10, 20, 30, 40, 50 ] 
gamma_cdp = [ 11.07, 6.2 , 4.45 , 4 , 2.875 ]
gamma_gopa = [9.6, 6.6, 2.5, 2.5,  2.3 ] 
gamma_ldp = [ 0.003, 0.0005, 0.0003, 5e-05, 0.01 ]

# setup 
n_workers=10000 # n. of users 
n_runs=10 # evaluation over 10 tests

epsp=1 # global epsilon 
rho=0.5 
nH = nc.getHonestUsers(n_workers, rho)
deltapp = 1/(nH**2)
kappa=10 


class PlotData:
    def __init__(self, seed): 
        self.accl_gopa =  [] 
        self.objl_gopa  = [] 
        self.accl_cdp = []
        self.objl_cdp = [] 
        self.accl_ldp = [] 
        self.objl_ldp = [] 
        self.seed = seed
    
seed = 32

# ~ if (os.path.isfile('PlotData_'+str(seed)+'.pickle')):
if (len(sys.argv) != 2): 
    print("usage: python gen.py mode")
    print("modes:") 
    print("\t l: load stored results and plot them")
    print("\t t: train, test and plot results")
    exit()

if (sys.argv[1] == 'l'): 
    # if data is already stored, then load it 
    with open('PlotData_'+str(seed)+'.pickle', 'rb') as handle:
        pltd = pickle.load(handle)
        handle.close()
    
elif (sys.argv[1] == 't'): 
    
    pltd = PlotData(32) #
    # Train an evaluate our model for tuned parameters of Gamma 
    seed1 = hash(pltd.seed) 
    seed2 = hash(seed1) 
    seed3 = hash(seed2)

    for i in range(len(n_iter_l)): 
        print("Eval CDP on gamma=", gamma_cdp[i], " n_runs= ", n_runs, " n_iter=", n_iter_l[i]) 
        accll_cdp, objll_cdp, _ ,  _ , _ = sgd.testCDP(n_runs, n_iter_l[i], n_workers, gamma_cdp[i], epsp, deltapp,  hash(seed1+1), False)
        print("Test Accuracy: mean= ", accll_cdp.mean(), ", std= ", accll_cdp.std())
        pltd.accl_cdp.append( (accll_cdp.mean(), accll_cdp.std()) )
        print("Objective Function: mean= ", objll_cdp.mean(), ", std= ", objll_cdp.std())
        pltd.objl_cdp.append( (objll_cdp.mean(), objll_cdp.std()) ) 
        print(" ")
        
        print("Eval Gopa on gamma=", gamma_gopa[i], " n_runs= ", n_runs, " n_iter=", n_iter_l[i]) 
        accll_gopa, objll_gopa, _ ,  _ , _ = sgd.testGopa(n_runs, n_iter_l[i], n_workers, gamma_gopa[i], epsp, deltapp, rho, kappa,  hash(seed2+i), False)
        pltd.accl_gopa.append(  (accll_gopa.mean(), accll_gopa.std())  )
        print("Test Accuracy: mean= ", accll_gopa.mean(), ", std= ", accll_gopa.std())
        pltd.objl_gopa.append(  (objll_gopa.mean(), objll_gopa.std()) ) 
        print("Objective Function: mean= ", objll_gopa.mean(), ", std= ", objll_gopa.std())
        print(" ")
        
        print("Eval LDP on gamma=", gamma_ldp[i], " n_runs= ", n_runs, " n_iter=", n_iter_l[i]) 
        accll_ldp, objll_ldp, _ ,  _ , _ = sgd.testLDP(n_runs, n_iter_l[i], n_workers, gamma_ldp[i], epsp, deltapp,  hash(seed3+i), False)
        pltd.accl_ldp.append( (accll_ldp.mean(), accll_ldp.std() ) )
        print("Test Accuracy: mean= ", accll_ldp.mean(), ", std= ", accll_ldp.std())
        pltd.objl_ldp.append( (objll_ldp.mean(), objll_ldp.std() ) )
        print("Objective Function: mean= ", objll_ldp.mean(), ", std= ", objll_ldp.std())
        print(" ")
    
else:
    print("usage: python gen.py mode")
    print("modes:") 
    print("\t l: load stored results and plot them")
    print("\t t: train, test and plot results")
    exit()
    
    # store data
    # ~ with open('PlotData_'+str(seed)+'.pickle', 'wb') as handle:
        # ~ pickle.dump(pltd, handle, protocol=pickle.HIGHEST_PROTOCOL)
        # ~ handle.close()
# ~ print(pltd)

linestyle_ldp =        (0, (3, 5, 1, 5, 1, 5))
linestyle_cdp =        (0, (3, 1, 1, 1, 1, 1))

# now plot results
plt.xlabel("Number of iterations")
plt.ylabel("Test accuracy")
plt.errorbar(n_iter_l, [ pltd.accl_cdp[i][0] for i in range(len(n_iter_l)) ], yerr = [ pltd.accl_cdp[i][1] for i in range(len(n_iter_l)) ], color='tab:orange', linestyle=linestyle_cdp, linewidth=3, label=r'FedSGD w. trusted curator (Central DP)')
plt.errorbar(n_iter_l, [ pltd.accl_gopa[i][0] for i in range(len(n_iter_l)) ], yerr= [ pltd.accl_gopa[i][1] for i in range(len(n_iter_l)) ], color='tab:blue', label=r'FedSGD w. GOPA ($\rho=0.5$)', linewidth=3)
plt.errorbar(n_iter_l, [ pltd.accl_ldp[i][0] for i in range(len(n_iter_l)) ], yerr =  [ pltd.accl_ldp[i][1] for i in range(len(n_iter_l)) ], color='tab:green', linestyle=linestyle_ldp, linewidth=3, label=r'FedSGD w. local DP')
plt.legend()
plt.show() 

# ~ plt.xlabel("Number of epochs")
# ~ plt.ylabel("Objective function")
# ~ plt.errorbar(n_iter_l,   [ pltd.objl_cdp[i][0] for i in range(len(n_iter_l)) ], yerr = [ pltd.objl_cdp[i][1] for i in range(len(n_iter_l)) ], color='tab:orange', label=r'FedSGD w. trusted curator (Central DP)')
# ~ plt.errorbar(n_iter_l, [ pltd.objl_gopa[i][0] for i in range(len(n_iter_l)) ], [ pltd.objl_gopa[i][1] for i in range(len(n_iter_l)) ], color='tab:blue', label=r'FedSGD w. GOPA ($\rho=0.5$)')
# ~ plt.errorbar(n_iter_l, [ pltd.objl_ldp[i][0] for i in range(len(n_iter_l)) ], yerr =  [ pltd.objl_ldp[i][1] for i in range(len(n_iter_l)) ], color='tab:green', label=r'FedSGD w. local DP')
# ~ plt.legend()
# ~ plt.show() 


# ~ return  (pltd.accl_cdp, pltd.objl_cdp), (pltd.accl_gopa, objl_gopa), (pltd.accl_ldp, objl_ldp )



