import numpy as np
import scipy as sp
import math as m 
import matplotlib.pyplot as plt
import pickle

from sklearn.datasets import fetch_openml
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import Normalizer
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import LabelBinarizer

plt.rcParams.update({'font.size': 14})

import noisecalibration as nc

X, y = fetch_openml(name='houses', version=2, return_X_y=True, as_frame=False)
n, d = X.shape
print(n, d)
c = np.unique(y)    
y[y==c[0]] = -1
y[y==c[1]] = 1
y = y.astype(float)
#Scale and normalize data
standardizer = StandardScaler()
X = standardizer.fit_transform(X)
normalizer = Normalizer()
X = normalizer.transform(X)

X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=.80, random_state=42, stratify=y)
print(X_train.shape, y_train.shape, X_test.shape, y_test.shape)
n_train = X_train.shape[0]

    
from sklearn.linear_model._base import LinearClassifierMixin, SparseCoefMixin, BaseEstimator
from sklearn.utils.extmath import log_logistic, safe_sparse_dot
from sklearn.linear_model._logistic import _intercept_dot
from scipy.special import expit
from sklearn.utils.validation import check_X_y

   
def my_logistic_obj_and_grad(theta, X, y, lamb):
    """
    Computes the value and gradient of the objective function of logistic 
    regression defined as:
    min (1/n) \sum_i log_loss(theta;X[i,:],y[i]) + (lamb / 2) \|w\|^2,
    where theta = w (if no intercept), or theta = [w b] (if intercept)

    Parameters
    ----------
    theta: array, shape (d,) or (d+1,)
        The initial value for the model parameters. When an intercept is used, it corresponds to the last entry
    X : array, shape (n, d)
        The data
    y : array, shape (n,)
        Binary labels (-1, 1)
    lamb : float
        The L2 regularization parameter


    Returns
    -------
    obj : float
        The value of the objective function
    grad : array, shape (d,) or (d+1,)
        The gradient of the objective function
    """
    n_samples, n_features = X.shape
    grad = np.empty_like(theta)
    

    w, c, yz = _intercept_dot(theta, X, y)

    # Logistic loss is the negative of the log of the logistic function
    obj = -np.mean(log_logistic(yz)) + .5 * lamb * np.dot(w, w)

    z = expit(yz)
    z0 = (z - 1) * y

    grad[:n_features] = safe_sparse_dot(X.T, z0) / n_samples + lamb * w

    # Case where we fit the intercept
    if grad.shape[0] > n_features:
        grad[-1] = z0.sum() / n_samples
    return obj, grad
   
   
def my_logistic_obj_and_gradvec(theta, X, y):
    """
    Vectorized version of my_logistic_obj_and_grad. In our experiments we don't 
    use regularization, so this function does not require regularization as a parameter.
    The difference with my_logistic_obj_and_grad is that my_logistic_obj_and_gradvec
    computes one gradient per point of X. 
    
    Parameters
    ----------
    theta: array, shape (d,) or (d+1,)
        The initial value for the model parameters. When an intercept is used, it corresponds to the last entry
    X : array, shape (n, d)
        The data
    y : array, shape (n,)
        Binary labels (-1, 1)


    Returns
    -------
    obj : float
        The value of the objective function
    gradl : array of shape (X.size,) of arrays of shape (d,) or (d+1,)
        one for each point of X used in this step of the training
    """
    n_samples, n_features = X.shape
    gradl = np.empty((n_samples, theta.shape[0]))

    w, c, yz = _intercept_dot(theta, X, y)

    # Logistic loss is the negative of the log of the logistic function
    obj = -np.mean(log_logistic(yz)) 
    z = expit(yz)
    z0 = (z - 1) * y
    gradl[:,:n_features] =  np.array( [X[i] * z0[i] for i in np.arange(n_samples) ] )
    # Case where we fit the intercept
    if gradl.shape[1] > n_features:
        gradl[:,-1] = z0
    return obj, gradl

        
        
def sgd_globalgrad(X, y, gamma, n_iter, obj_and_grad,  obj_and_gradvec,  theta_init, 
                n_workers, n_batch=1, variance=0, freq_obj_eval=10,
                n_obj_eval=1000, random_state=None):
    """Gopa Stochastic Gradient Descent (SGD) algorithm

    Parameters
    ----------
    X : array, shape (n, d)
        The data
    y : array, shape (n,)
        Binary labels (-1, 1).
    gamma : float | callable
        The step size. Can be a constant float or a function
        that allows to have a variable step size
    n_iter : int
        The number of iterations
    obj_and_grad : callable
        A function which takes as a vector of shape (p,), a dataset of shape (n_batch, d)
        and a label vector of shape (n_batch,), and returns the objective value and gradient.
    obj_and_gradvec: callable
        The same as obj and grad, but returns a vector of gradients instead of the
        aggregated gradient 
    theta_init : array, shape (p,)
        The initial value for the model parameters
    n_workers: int
        the number of parties that will participate in training
    n_batch : int
        Size of the mini-batch to use at each iteration of SGD.
    variance: float
        Variance of the Gaussian noise added to each gradient
    freq_obj_eval : int
        Specifies the frequency (in number of iterations) at which we compute the objective
    n_obj_eval : int
        The number of points on which we evaluate the objectuve
    random_state : int
        Random seed to make the algorithm deterministic


    Returns
    -------
    theta : array, shape=(p,)
        The final value of the model parameters
    obj_list : list of length (n_iter / freq_obj_eval)
        A list containing the value of the objective function computed every freq_obj_eval iterations
    """
    rng = np.random.RandomState(random_state)
    n_samples, d = X.shape
    p = theta_init.shape[0]
    
    
    # assert(n_samples  >= n_workers)
    # Each user has either floor(n_samples / n_workers)  or ceil(n_samples / n_workers) points 
    
    ppuser = m.floor(n_samples/n_workers) # each user has at least ppuser points 
    
    n2 = n_samples - n_workers * ppuser #first n2 users have ppuser+1 points
    n1 = n_workers - n2 # next n1 users have ppuser points
    
    idxs = np.arange(n_samples) # id of the points 
    
    # assign points to each user
    idxs_part = [] 
    # user i has points idxs_part[i] 
    for i in range(n2):
        idxs_part.append(idxs[(ppuser+1)*i: (ppuser+1)*(i+1)])
    
    for i in range(n1):
        idxs_part.append(idxs[ppuser*(n2+i): ppuser*(n2+i+1)])
  
    theta = theta_init.copy()
    
    # if a constant step size was provided, we turn it into a constant function
    # (stepsize is constant in Gopa)
    if not callable(gamma):
        def gamma_func(t):
            return gamma
    else:
        gamma_func = gamma
    
    # we draw a fixed subset of points to monitor the objective
    idx_eval = rng.randint(0, n_samples, n_obj_eval)
    
    # list to record the evolution of the objective (for plotting)
    obj_list = []
    
    # perform the descent
    for t in range(n_iter):
        if t % freq_obj_eval == 0:
            # evaluate objective
            # we might just drop the gradient computation from obj_and_grad
            obj, _ = obj_and_grad(theta, X[idx_eval, :], y[idx_eval])
            obj_list.append(obj)
      
        # at each update, each user takes one of its points at random and use it to compute
        #  its gradient update
        idxlist = np.array( [ rng.choice(idxs_part[u]) for u in  np.arange(n_workers) ] )
        
        # compute the list of gradients for the selected points 
        _, gradl = obj_and_gradvec(theta, X[idxlist, :] , y[idxlist])
        
        # obtain the final gradient, add DP noise and update
        gradsum = sum(gradl) 
        perturbed_grad = gradsum/n_workers + rng.normal(scale=m.sqrt(variance), size=p) 
        theta -= gamma_func(t+1) * perturbed_grad
    
    # evaluate the objective with the final parameters
    obj, _ = obj_and_grad(theta, X, y)
    return theta, obj, obj_list
 
 

class DistributedPrivateSGDLogisticRegression(BaseEstimator, LinearClassifierMixin, SparseCoefMixin):
    """Our sklearn estimator for private logistic regression defined as:
    min (1/n) \sum_i log_loss(theta;X[i,:],y[i]) + (lamb / 2) \|w\|^2,
    where theta = [w b]
    
    Parameters
    ----------
    variance : float
        The variance used to perturb the output of the gradient private aggregation
    gamma : float | callable
        The step size. Can be a constant float or a function
        that allows to have a variable step size
    n_iter : int
        The number of iterations
    n_workers: int
        The number of users participating in training
    lamb : float
        The L2 regularization parameter
    n_batch : int
        Size of the mini-batch to use at each iteration of SGD.
    freq_obj_eval : int
        Specifies the frequency (in number of iterations) at which we compute the objective
    n_obj_eval : int
        The number of points on which we evaluate the objectuve
    random_state : int
        Random seed to make the algorithm deterministic

        
    Attributes
    ----------
    coef_ : (p,)
        The weights of the logistic regression model.
    intercept_ : (1,)
        The intercept term of the logistic regression model.
    obj_list_: list of length (n_iter / freq_obj_eval)
        A list containing the value of the objective function computed every freq_loss_eval iterations
    """
    
    def __init__(self, variance, gamma, n_iter, n_workers,  lamb=0, n_batch=1, freq_obj_eval=10, n_obj_eval=1000, random_state=None):
        self.variance = variance
        self.gamma = gamma
        self.n_iter = n_iter
        self.lamb = lamb
        self.n_batch = n_batch
        self.freq_obj_eval = freq_obj_eval
        self.n_obj_eval = n_obj_eval
        self.random_state = random_state
        self.n_workers = n_workers
        
    
    def fit(self, X, y):
        # check data and convert classes to {-1,1} if needed
        X, y = self._validate_data(X, y, accept_sparse='csr', dtype=[np.float64, np.float32], order="C")
        self.classes_ = np.unique(y)    
        y[y==self.classes_[0]] = -1
        y[y==self.classes_[1]] = 1
                
        n_samples, p = X.shape
        theta_init = np.zeros(p+1) # initialize parameters to zero
        
        # define the functions for value and gradient needed by SGD
        obj_grad = lambda theta, X, y: my_logistic_obj_and_grad(theta, X, y, lamb=self.lamb)
        obj_gradvec = lambda theta, X, y: my_logistic_obj_and_gradvec(theta, X, y)
        
        # perform gradient descent and recover final model and objective function
        # values in the process
        theta, obj, obj_list = sgd_globalgrad(X, y, self.gamma, 
                                        self.n_iter, obj_grad, 
                                        obj_gradvec, theta_init, 
                                        self.n_workers, self.n_batch,
                                        self.variance, self.freq_obj_eval, 
                                        self.n_obj_eval, self.random_state)
        
        # save the learned model into the appropriate quantities used by sklearn
        self.intercept_ = np.expand_dims(theta[-1], axis=0)
        self.coef_ = np.expand_dims(theta[:-1], axis=0)

        # also save list of objective values during optimization for plotting
        self.obj_list_ = obj_list
        self.obj = obj
        return self


def test(n_runs, n_iter, n_workers, gamma, var, seed, doplot):
    """
    Evaluate n_runs executions of the DistributedPrivateSGDLogisticRegression. 
    
    Paramters:
    ----------
    n_runs: int
        number of runs in which we train a model and test its accuracy
    n_iter: int
        number of iterations of each run 
    n_wokers: int
        number of parties (users in Gopa)
    gamma: float 
        gradient descent step size
    var: float 
        noise variance per component to satisfy differential privacy 
    seed: int
        random seed
    doplot: bool
        flag to specify if the objective function should be plotted for each
        run 
        
    Returns:
    --------
    accuracyl: array of shape (n_runs,) of int 
        The array of accuracies at each run
        
    objl: an array of int of shape (n_runs,)
        The final value of the objective function at each run
        
    obj_list_list: a list of shape (n_runs,) of lists of shape (n_iter,)
        The value of the objective function in the monitored iterations
        for each run
        
    iter_list_list:  a list of shape (n_runs,) of lists of shape (n_iter,)
        The number of the iterations that were monitored at each run    
    """ 
    
    lamb=0 # we do no regularization 
    n_batch=1 
    # init objects to store accuracy and objective fcn values after each run
    accuracyl= np.zeros(n_runs) 
    objl = np.zeros(n_runs) 
    obj_list_list = [] # snapshot of the obj function during descent
    iter_list_list = [] # iterations where the snapshots were taken (this only makes sense when the snapshot is not taken at each iteration, however we save the entire descent here)
    # start tests 
    for i in range(n_runs): 
        mlr = DistributedPrivateSGDLogisticRegression(var, 
                        gamma, n_iter, n_workers, lamb,
                        n_batch,freq_obj_eval=1, random_state=seed+i)
        mlr.fit(X_train, y_train)
        accuracyl[i] = mlr.score(X_test, y_test) 
        obj_list = mlr.obj_list_
        objl[i] = mlr.obj
        iter_list = np.arange(len(obj_list)) * mlr.freq_obj_eval

        # if doplot flag is on, plot the objective function
        if doplot: 
            plt.ylabel("Objective function") 
            plt.xlabel("N of iterations")
            plt.plot(iter_list, obj_list)
            plt.show()
            
        obj_list_list.append(obj_list)
        iter_list_list.append(iter_list)
    
    return accuracyl, objl, obj_list_list, iter_list_list

    
def testCDP(n_runs, n_iter, n_workers, gamma, epsp, deltapp, seed, doplot):
    """ 
    Evaluate Fed SGD with Central Differentiall Privacy (trusted curator) 
    on n_runs times on the specified parameters. 
    Each evaluation is perform the training phase, then evaluate the model
    and keep the scores
    
    Parameters: 
    ----------
    n_runs: int 
        number of evaluations
    n_iter: int
        number of federated learning iterations, where each iteration
        is taking the update of all users and aggregating it
    gamma: float
        learning rate
    epsp: float
        final epsilon DP after all iterations
    deltapp: float
        final delta DP after all iterations
    seed: int 
        seed of the randomized computations
    doplot: bool
        flag to specify if the objective function should be plotted for each
        run 
        
        
    Returns: 
    --------
    accuracyl: an array of int of shape (n_iter,)
        The test accuracy of each run
    
    objl: an array of int of shape (n_runs,)
        The final value of the objective function at each run
        
    obj_list_list: a list of shape (n_runs, n_iter,)
        The value of the objective function in the monitored iterations
        for each run
        
    iter_list_list:  a list of shape (n_runs, n_iter,)
        The number of the iterations that were monitored at each run
    
    params: dictionary
        the list of all parameters used in sgd (for debugging)
    """ 
    # compute DP budget for each step
    eps, delta = nc.advancedComposition(epsp, deltapp, n_iter) 

    # compute the required variance in order to satisfy DP
    # our sensitivity is 2, therefore we must double the standard deviation
    # or equivalently, multiply the variance by 4
    var = 4 * nc.total_noise_var_CentralDP(n_workers, eps, delta) 
    
    # run tests 
    accuracyl, objl, obj_list_list, iter_list_list = test(n_runs, n_iter, n_workers, gamma, var, seed, doplot)
        
    params = { 'gamma': gamma, 
               'n_iter': n_iter, 
               'epsilonp': epsp, 
               'deltapp': deltapp, 
               'variance': var, 
               'eps': eps, 
               'delta':delta, 
               'n_workers':n_workers }
    
    return accuracyl, objl, obj_list_list, iter_list_list, params
   

def testLDP(n_runs, n_iter, n_workers, gamma, epsp, deltapp, seed, doplot):
    lamb=0
    n_batch=1
    eps, delta = nc.advancedComposition(epsp, deltapp, n_iter) 
    var = 4* nc.total_noise_var_LocalDP(n_workers, eps, delta)

    accuracyl, objl, obj_list_list, iter_list_list = test(n_runs, n_iter, n_workers, gamma, var, seed, doplot)
        
    params = { 'gamma': gamma, 
               'n_iter': n_iter, 
               'epsilonp': epsp, 
               'deltapp': deltapp, 
               'variance': var, 
               'eps': eps, 
               'delta':delta, 
               'n_workers':n_workers }
               
    return accuracyl, objl, obj_list_list, iter_list_list, params
    
    
    
def testGopa(n_runs, n_iter, n_workers, gamma, epsp, deltapp, rho, kappa, seed, doplot):
    lamb=0
    n_batch=1
    eps, delta = nc.advancedComposition(epsp, deltapp, n_iter) 
    nH = nc.getHonestUsers(n_workers, rho) 
    var = 4* nc.total_noise_var_GOPA(n_workers, nH, eps, delta, nc.GTYPE_KOUT, kappa)
  
    accuracyl, objl, obj_list_list, iter_list_list = test(n_runs, n_iter, n_workers, gamma, var, seed, doplot)
  
    params = { 'gamma': gamma , 'n_iter': n_iter, 'epsilonp': epsp, 
                deltapp: 'deltapp', 'variance': var, 'eps': eps, 
                'delta':delta, 'n_workers':n_workers, 'rho':rho, 
                'kappa':kappa }
    
    return accuracyl, objl, obj_list_list, iter_list_list, params


""" 
Functions for tuning gamma 
"""
def expgamma_cdp(n_runs, n_iter, n_workers, gammal, epsp, deltapp, seed): 
    printparams = True
    objmeanl = []
    objstdl = [] 
    seed = hash(seed)
    for i in range(len(gammal)):
        _ , objl, _ ,  _ , params = testCDP(n_runs, n_iter, n_workers, gammal[i], epsp, deltapp,  hash(seed+i), False)
        if printparams:
            print("Tuning gamma for CDP (n_runs = ", n_runs ,"n_iter=", str(n_iter)+"):")
            print(params) 
            printparams = False
        print("gamma=", gammal[i], " obj mean= ", objl.mean(), "obj std= ", objl.std())
        objmeanl.append(objl.mean())
        objstdl.append(objl.std()) 
    return objmeanl, objstdl
    

def expgamma_gopa(n_runs, n_iter, n_workers, gammal,  epsp, deltapp, rho, kappa, seed):
    printparams = True
    objmeanl = []
    objstdl = [] 
    seed = hash(seed)
    for i in range(len(gammal)):
        _ , objl, _ ,  _ , params= testGopa(n_runs, n_iter, n_workers, gammal[i], epsp, deltapp, rho, kappa,  hash(seed+i), False)
        if printparams:
            print("Tuning gamma for Gopa (n_runs = ", n_runs , "n_iter=", str(n_iter)+"):")
            print(params) 
            printparams = False
        print("gamma=", gammal[i], " obj mean= ", objl.mean(), "obj std= ", objl.std())
        objmeanl.append(objl.mean())
        objstdl.append(objl.std()) 
    return objmeanl, objstdl
    

def expgamma_ldp(n_runs, n_iter, n_workers, gammal, epsp, deltapp, seed):
    printparams = True
    objmeanl = []
    objstdl = [] 
    seed = hash(seed)
    for i in range(len(gammal)):
        _ , objl, _ ,  _ , params = testLDP(n_runs, n_iter, n_workers, gammal[i], epsp, deltapp,  hash(seed+i), False)
        if printparams:
            print("Tuning gamma for LDP (n_runs = ", n_runs , ") :")
            print(params) 
            printparams = False
        print("gamma=", gammal[i], " obj mean= ", objl.mean(), "obj std= ", objl.std())
        objmeanl.append(objl.mean())
        objstdl.append(objl.std()) 
    return objmeanl, objstdl


