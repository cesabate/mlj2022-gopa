import matplotlib.pyplot as plt 
import pickle
import numpy as np
import noisecalibration as nc
import math as m

plt.rcParams.update({'font.size': 14})


""" 
Utilitary functions for plotting
"""     
    
def plot_CDP(n, eps, delta, plt):
    CDP_var = nc.total_noise_var_CentralDP(n,eps, delta)
    plt.axhline(CDP_var, linestyle='--', label=r'Central DP with $n$ users', color='y')
    
    
def plot_LDP(n, eps, delta, plt):
    LDP_var = nc.total_noise_var_LocalDP(n, eps, delta) 
    plt.axhline(LDP_var, linestyle='--', label=r'Local DP $n$ users', color='r')

def plot_est_emp_total_var_dropout_gamma(n, rho, eps, delta ,kappa):
    gamma_v = np.arange(0, 0.05, 0.001) 
    kl = [10,20,30,40] 
    GOPA_var_vl = [] 
    
    for i in range(len(kl)):
        # The parameter gamma below is the proportion of users that dropped out
        GOPA_var_v = [ nc.estimate_total_noise_var_GOPA_dropouts(n, kl[i], gamma,rho, eps, delta, kappa) for gamma in gamma_v ]
        plt.plot(gamma_v, GOPA_var_v, label='GOPA, k='+ str(kl[i]) + " (est)", linestyle=":")
    gamma_emp=[0.1, 0.05, 0.01, 0.001] 
    for i in range(len(kl)):
        # The parameter gamma below is the proportion of users that dropped out
        GOPA_var_v_emp = [ nc.GOPA_total_var_dropouts_empirical1(n, kl[i], gamma, rho, eps, delta,  kappa) for gamma in gamma_emp ]
        plt.plot(gamma_emp, GOPA_var_v_emp, label='GOPA, k='+ str(kl[i])+' (emp)') 
    
    plt.xlabel(r'Proportion of user that dropped out $\gamma$')
    plt.ylabel('Total Variance') 
    plot_CDP(n, eps, delta, plt)
    plot_LDP(n, eps, delta, plt)
    plt.legend()
    plt.show()

def plot_emp_total_var_dropout_gamma(n, rho, eps, delta ,kappa):
    gamma_emp=[0.1, 0.05, 0.01, 0.001] 
    k_emp = [20, 30, 40] 
    for i in range(3):
        # The paramerter gamma below is the proportion of users that dropped out
        GOPA_var_v_emp = [ nc.GOPA_total_var_dropouts_empirical1(n, k_emp[i], gamma, rho, eps, delta,  kappa) for gamma in gamma_emp ]
        plt.plot(gamma_emp, GOPA_var_v_emp, label='GOPA, k='+ str(k_emp[i])+' (emp)') 
    plt.xlabel(r'Proportion of user that dropped out $\gamma$')
    plt.ylabel('Total Variance') 
    plot_CDP(n, eps, delta, plt)
    plot_LDP(n, eps, delta, plt)
    plt.legend()
    plt.show()
    
def plot_emp_total_var_dropout_gamma2(n, rho):
    eps = 0.1
    nH = nc.getHonestUsers(n, rho)
    deltap = (1/(nH**2))
    delta = 10 * deltap
    rho_k_l =  dict([ (50, [20, 40]), (100, [10,20]) ])
    gamma_emp=[0.1, 0.05, 0.01, 0.001] 
    rho_key = round(rho*100)
    if rho_key not in rho_k_l: 
        print("Invalid value for rho")
        return -1
    for i in [0,1]:
        # The parameter gamma below is the proportion of users that dropped out
        GOPA_var_v_emp = [ nc.GOPA_total_var_dropouts_empirical2(n, rho_k_l[rho_key][i], gamma, rho) for gamma in gamma_emp ]
        plt.plot(gamma_emp, GOPA_var_v_emp, label=r'GOPA, $\rho$='+str(rho)+', k='+ str(rho_k_l[rho_key][i])+' (emp2)') 
    plt.xlabel(r'Proportion of user that dropped out $\gamma$')
    plt.ylabel('Total Variance') 
    plot_CDP(n, eps, delta, plt)
    plot_LDP(n, eps, delta, plt)
    plt.legend()
    plt.show()
    
    
def plot_total_var_dropout_kappa(n, rho, eps, delta, gamma, low, high, step):
    kappa_v = np.arange(low, high, step) 
    k_l = [m.floor(n/100), m.floor(n/50), m.floor(n/10)]
    plt.xlabel(r'Parameter $kappa$')
    plt.ylabel('Total Variance') 
    GOPA_var_vl = []
    for i in  [0,1,2]:
        GOPA_var_vl.append([ nc.estimate_total_noise_var_GOPA_dropouts(n, k_l[i], gamma,rho, eps, delta, kappa) for kappa in kappa_v ])
        plt.plot(kappa_v, GOPA_var_vl[i], label='GOPA, k='+str(k_l[i])) 
    plot_CDP(n, eps, delta, plt)
    plot_LDP(n, eps, delta, plt)
    plt.legend()
    plt.show()
    

def plot_total_var_dropout_k(n, rho, eps, delta, kappa, klow, khigh, kstep):
    k_v = np.arange(klow, khigh, kstep) 
    gamma_l = [0.001, 0.01, 0.05]
    plt.xlabel(r'k')
    plt.ylabel('Total Variance') 
    GOPA_var_vl = []
    for i in  [0,1,2]:
        GOPA_var_vl.append([ nc.estimate_total_noise_var_GOPA_dropouts(n, k, gamma_l[i],rho, eps, delta, kappa) for k in k_v ])
        plt.plot(k_v, GOPA_var_vl[i], label=r'GOPA, $\gamma$='+str(gamma_l[i])) 
    plot_CDP(n, eps, delta, plt)
    plot_LDP(n, eps, delta, plt)
    plt.legend()
    plt.show()


def plotTotalVarianceOverRho(n, eps, delta, kappa, alsononkout, ldp):    
    """
    Plots the variance of the average estimation CDP and Gopa for k-out Graphs 
    (see Corollary 1) with n users, 
    to obtain (eps,delta)-DP. Gopa is plotted for all values of rho between 
    0.3 and 1. 
    
    Additionally, if the flag ldp is set to True, it also
    plots the variance of LDP for the above parameters (that apply for LDP). 
    If the flag alsononkout is set to True, it plots the variance of Gopa
    for arbitrarily connected or complete Graphs, which is the same as
    we don't consider residual pairwise noise. 
    
    Parameters:
    -----------
    n: int
        number of users
    eps: float
        epsilon parameter of DP
    delta: float
        delta parameter of DP
    kappa: float
        kappa parameter of Corollary 1
    alsononkout: bool
        if True, it plots the variance of Gopa for non-k-out graphs 
    ldp: bool
        if True, it also plots the variance of LDP 
    """

    rho_v = np.arange(0.3, 1, 0.05) # vector of values of rho
    nH_v = [ nc.getHonestUsers(n, rho) for rho in rho_v ]
    
    GOPA_var_kout_v = [ nc.total_noise_var_GOPA(n, nH, eps, delta,  nc.GTYPE_KOUT, kappa) for nH in nH_v ]
    GOPA_var_nonkout_v = [ nc.total_noise_var_GOPA(n, nH, eps, delta,  nc.GTYPE_COMPLETE, kappa) for nH in nH_v ]
    CDP_var = nc.total_noise_var_CentralDP(n,eps, delta)
    LDP_var = nc.total_noise_var_LocalDP(n, eps, delta) 
    
       
    plt.yscale('log')
    plt.ylabel("Variance of estimated average")
    plt.xlabel(r'Proportion of honest users $\rho$ that did not drop out' )
    plt.plot(rho_v, GOPA_var_kout_v, label='GOPA ($k$-out)')
    if (alsononkout):
        plt.plot(rho_v, GOPA_var_nonkout_v, label='GOPA (complete or path)')
    
    plt.axhline(CDP_var, linestyle='--', label=r'Central DP with $n$ users', color='y')
    if (ldp):
        plt.axhline(LDP_var, linestyle='--', label=r'Local DP $n$ users', color='r')
    plt.title(r'$\varepsilon$='+ str(eps) + ', $\delta$=' + str(delta) +', $\kappa$='+str(kappa)) 
    plt.legend()
    plt.show()
    



def plotTotalVarianceOverRhoKappa(n, eps, delta, kappav, gtype):  
    """
    Plot de final estimation variances of CDP, LDP and Gopa  
    (see Corollary 1) with n users, 
    to obtain (eps,delta)-DP. Gopa is plotted for all values of rho between 
    0.3 and 1, and the values of kappa in kapav. 
    
    If the flag alsononkout is set to True, it plots the variance of Gopa
    for arbitrarily connected or complete Graphs, which is the same as
    we don't consider residual pairwise noise. 
    
    Parameters:
    -----------
    n: int
        number of users
    eps: float
        epsilon parameter of DP
    delta: float
        delta parameter of DP
    kappav: float
        kappa parameter of Corollary 1
    gtype: eiter GTYPE_KOUT or GTYPE_COMPLETE or GTYPE_CONNECTED (defined in noisecalibration.py )
        type of communication graph (see Corollary 1)
    """
    
    rho_v = np.arange(0.3, 1, 0.05)
    nH_v = [ nc.getHonestUsers(n, rho) for rho in rho_v ]
    
    GOPA_var_vv = [ ([ nc.total_noise_var_GOPA(n, nH, eps, delta,  gtype, kappa) 
                                for nH in nH_v ]) 
                            for kappa in kappav ]
        
    CDP_nH_v = [ nc.total_noise_var_CentralDP(nH, eps, delta) for nH in nH_v ]
    
    CDP_var = nc.total_noise_var_CentralDP(n,eps, delta)
    LDP_var = nc.total_noise_var_LocalDP(n, eps, delta) 
   
    if gtype == nc.GTYPE_KOUT:
        gtypelabel = '$k$-out' 
    else:
        gtypelabel = 'path or complete' 
        
    # line patterns, if needed
    linestylev = [  (0, (1, 10)), 
                    (0, (1, 1)), 
                    (5, (10, 3)), 
                    (0, (5, 5)),
                    (0, (5, 1)),
                    (0, (3, 10, 1, 10)),
                    (0, (3, 5, 1, 5)),
                    (0, (3, 1, 1, 1)),
		           (0, (3, 10, 1, 10, 1, 10))]
                      
                      
    linestyle_ldp =        (0, (3, 5, 1, 5, 1, 5))
    linestyle_cdp =        (0, (3, 1, 1, 1, 1, 1))
   
    plt.yscale('log')
    plt.ylabel("Variance of estimated average")
    plt.xlabel(r'Proportion of honest users $\rho$ that did not drop out' )
    for kidx in range(len(kappav)): 
        plt.plot(rho_v,  GOPA_var_vv[kidx], linestyle=linestylev[kidx], linewidth=2,label='GOPA, w. $kappa$='+str(kappav[kidx]))
    plt.axhline(CDP_var, linestyle=linestyle_cdp, linewidth=2,label=r'Central DP with $n$ users', color='y')
    plt.axhline(LDP_var, linestyle=linestyle_ldp, linewidth=2,label=r'Local DP w. $n$ users', color='r')
    plt.title(r'$\varepsilon$='+ str(eps) + ', $\delta$=' + str(delta)) 
    plt.legend()
    plt.show()
    


def plotTotalVarianceOverEpsilon(n, delta, kappa, elow, ehigh, estep):
    eps_v = np.arange(elow, ehigh, estep)
    rho_l = [0.3, 0.5, 0.7, 0.9]
    
    GOPA_var_nonkout_v = [nc.total_noise_var_GOPA(n, 0.9, eps, delta, nc.GTYPE_KOUT+1,kappa) for  eps in eps_v]
    CDP_var_v = [nc.total_noise_var_CentralDP(n, eps, delta) for eps in eps_v]
    plt.plot(eps_v, CDP_var_v, label='Central DP with $n$ users', linestyle='--', color='y')
    for i in range(len(rho_l)):
        nH = nc.getHonestUsers(n, rho_l[i])
        GOPA_var_kout_v = [nc.total_noise_var_GOPA(n, nH, eps, delta,nc.GTYPE_KOUT,kappa) for  eps in eps_v]
        plt.plot(eps_v, GOPA_var_kout_v, label=r'GOPA, $k$-out graph,  $\rho$='+str(rho_l[i]), )
    
    # ~ plt.plot(eps_v, GOPA_var_nonkout_v, label=r'GOPA, non k-out $\rho$=0.1')
    
    plt.ylabel('Variance of estimated average')
    plt.xlabel('$\epsilon$')
    plt.legend()
    plt.show()
    # #### Logarithmic scale
    LDP_var_v = [nc.total_noise_var_LocalDP(n, eps, delta) for eps in eps_v]
    plt.yscale('log')
    # ~ plt.plot(eps_v, GOPA_var_nonkout_v, label=r'GOPA, non k-out $\rho$=0.9')
    plt.plot(eps_v, CDP_var_v, label='Central DP with $n$ users', linestyle='--', color='y')
    plt.plot(eps_v, LDP_var_v, label='Local DP $n$ users', linestyle='--', color='r')
    for i in range(len(rho_l)):
        nH = nc.getHonestUsers(n, rho_l[i])
        GOPA_var_kout_v = [nc.total_noise_var_GOPA(n, nH, eps, delta,nc.GTYPE_KOUT,kappa) for  eps in eps_v]
        plt.plot(eps_v, GOPA_var_kout_v, label=r'GOPA, $k$-out graph, $\rho$='+str(rho_l[i]), )
   
    plt.ylabel('Variance of estimated average')
    plt.xlabel('$\epsilon$')
    plt.legend()
    plt.show()
    
def plot1b():
    n=10000
    delta=1/(n**2)
    kappa = 10
    nc.plotTotalVarianceOverEpsilon(n, delta, kappa, 0.1, 1, 0.01)    
    

# Plotting values of $\sigma_\Delta^2$ for different $k$
def plotPairwiseVarianceOverK(n, rho, eps, delta):
    nH = nc.getHonestUsers(n,rho)
    id_nv_kout_50 = nc.independent_noise_var_GOPA(nH, eps, delta, nc.GTYPE_KOUT, 50)
    id_nv_kout_100 = nc.independent_noise_var_GOPA(nH, eps, delta, nc.GTYPE_KOUT, 100)
    id_nv_kout_200 = nc.independent_noise_var_GOPA(nH, eps, delta, nc.GTYPE_KOUT, 200)
    id_nv_nonkout = nc.independent_noise_var_GOPA(nH, eps, delta, nc.GTYPE_KOUT+1, 100)
    k_v = np.arange(100, 1000, 10)
    pairwise_var_kout_v1  = [ nc.pairwise_noise_var_kout(rho, nH, k, 50, id_nv_kout_50) for k in k_v ]
    pairwise_var_kout_v2  = [ nc.pairwise_noise_var_kout(rho, nH, k, 100, id_nv_kout_100) for k in k_v ]
    pairwise_var_kout_v3  = [ nc.pairwise_noise_var_kout(rho, nH, k, 200, id_nv_kout_200) for k in k_v ]
    plt.plot(k_v, pairwise_var_kout_v1, label=r'$k$-out, $kappa$=50')
    plt.plot(k_v, pairwise_var_kout_v2, label=r'$k$-out, $kappa$=100')
    plt.plot(k_v, pairwise_var_kout_v3, label=r'$k$-out, $kappa$=200')
    #plt.axhline(pairwise_var_comp, label='complete', linestyle='--')
    plt.xlabel('k')
    plt.ylabel(' $\sigma_\Delta^2$')
    plt.legend()
    plt.show()
    pairwise_var_conn = nc.pairwise_noise_var_connected(nH, 100, id_nv_nonkout)
    pairwise_var_comp = nc.pairwise_noise_var_complete(100, id_nv_nonkout)
    print('Pairwise noise variance for a conected graph: ' + str(pairwise_var_conn))
    print('Pairwise noise variance for the complete graph: ' + str(pairwise_var_comp))
    # #### Logarithmic scale
    plt.yscale('log')
    plt.plot(k_v, pairwise_var_kout_v1, label=r'$k$-out, $kappa$=50')
    plt.plot(k_v, pairwise_var_kout_v2, label=r'$k$-out, $kappa$=100')
    plt.plot(k_v, pairwise_var_kout_v3, label=r'$k$-out, $kappa$=200')
    plt.axhline(pairwise_var_comp, label='Complete, $kappa$=100', linestyle='--', color='red')
    plt.axhline(pairwise_var_conn, label='Connected $kappa$=100', linestyle='--', color='green')
    plt.xlabel('$k$')
    plt.ylabel('$\sigma_\Delta^2$')
    plt.legend()
    plt.show()
    print('Pairwise noise variance for a conected graph: ' + str(pairwise_var_conn))
    print('Pairwise noise variance for the complete graph: ' + str(pairwise_var_comp))



# #### Values of $\sigma_\Delta^2$ depending on $\kappa$
def plotPairwiseVarianceOverKappa(n, rho, eps, delta):
    nH = nc.getHonestUsers(n,rho)
    kappa_v = np.arange(10, 200, 10)
    pairwise_var_kout_v1  = [ nc.pairwise_noise_var_kout(rho, nH, 100, kappa, nc.independent_noise_var_GOPA(nH, eps, delta, nc.GTYPE_KOUT, kappa)) for kappa in kappa_v ]
    pairwise_var_kout_v2  = [ nc.pairwise_noise_var_kout(rho, nH, 200, kappa, nc.independent_noise_var_GOPA(nH, eps, delta, nc.GTYPE_KOUT, kappa)) for kappa in kappa_v ]
    pairwise_var_kout_v3  = [ nc.pairwise_noise_var_kout(rho, nH, 500, kappa, nc.independent_noise_var_GOPA(nH, eps, delta, nc.GTYPE_KOUT, kappa)) for kappa in kappa_v ]
    pairwise_var_complete  = [ nc.pairwise_noise_var_complete(kappa, nc.independent_noise_var_GOPA(nH, eps, delta, nc.GTYPE_KOUT+1, kappa)) for kappa in kappa_v ]
    pairwise_var_connected = [ nc.pairwise_noise_var_connected(nH, kappa, nc.independent_noise_var_GOPA(nH, eps, delta, nc.GTYPE_KOUT+1, kappa)) for kappa in kappa_v ]
    plt.ylabel(r'$\sigma_\Delta^2$')
    plt.xlabel('kappa')
    plt.yscale('log')
    plt.plot(kappa_v, pairwise_var_kout_v1, label=r'$k$-out, $k$=100')
    plt.plot(kappa_v,pairwise_var_kout_v2, label=r'$k$-out, $k$=200')
    plt.plot(kappa_v,pairwise_var_kout_v3, label=r'$k$-out, $k$=400')
    plt.plot(kappa_v,pairwise_var_complete, label=r'Complete Graph', linestyle='--')
    plt.plot(kappa_v,pairwise_var_connected, label=r'Connected Graph', linestyle='--')
    plt.legend()
    plt.show()



def plotPairwiseVarianceOverEpsilon(n, rho, delta, kappa):
    nH = nc.getHonestUsers(n,rho)
    eps_v = np.arange(0.1, 0.5, 0.001)
    pairwise_var_kout_v1  = [ nc.pairwise_noise_var_kout(rho, nH, 250, kappa, 
                                    nc.independent_noise_var_GOPA(nH, eps, delta, nc.GTYPE_KOUT, kappa)) 
                                  for eps in eps_v ]
    pairwise_var_kout_v2  = [ nc.pairwise_noise_var_kout(rho, nH, 500, kappa, 
                                    nc.independent_noise_var_GOPA(nH, eps, delta, nc.GTYPE_KOUT, kappa)) 
                                  for eps in eps_v ]
    pairwise_var_kout_v3  = [ nc.pairwise_noise_var_kout(rho, nH, 1000, kappa, 
                                    nc.independent_noise_var_GOPA(nH, eps, delta, nc.GTYPE_KOUT, kappa)) 
                                  for eps in eps_v ]

    pairwise_var_complete  = [ nc.pairwise_noise_var_complete(kappa, 
                                                    nc.independent_noise_var_GOPA(nH, eps, delta, nc.GTYPE_KOUT+1, kappa)) 
                                                  for eps in eps_v ]
    pairwise_var_connected = [ nc.pairwise_noise_var_connected(nH, kappa, 
                                                    nc.independent_noise_var_GOPA(nH, eps, delta, nc.GTYPE_KOUT+1, kappa)) 
                                                  for eps in eps_v ]
    plt.yscale('log')
    plt.plot(eps_v, pairwise_var_kout_v1, label=r'$k$-out, k=250')
    plt.plot(eps_v, pairwise_var_kout_v2, label=r'$k$-out, k=500')
    plt.plot(eps_v, pairwise_var_kout_v3, label=r'$k$-out, k=1000')
    plt.plot(eps_v, pairwise_var_connected, label=r'$G^H$ Connected', linestyle='--')
    plt.plot(eps_v, pairwise_var_complete, label=r'$G^H$ Complete', linestyle='--')
    plt.xlabel(r'$\epsilon$')
    plt.ylabel(r'$\sigma_\Delta^2$')
    plt.legend()
    plt.show()






