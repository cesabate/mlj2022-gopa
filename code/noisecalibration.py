#!/usr/bin/env python
# coding: utf-8

import math as m
from mpmath import mp
import numpy as np

import random as rnd


"""
  Constants and other parameters
""" 

"""
 Types of graph, which determines different parameters in DP guarantees 
"""
GTYPE_CONNECTED = 1 # when G^H is arbitrarily connected
GTYPE_KOUT = 2 # when G is a k-out Graph
GTYPE_COMPLETE = 3 # when G^H is the complete graph

""" Constant a in Corollary 1 for different types of Graph """
a_KOUT = 3.75     
a_NONKOUT = 1.25 


""" 
Functions for noise calibration
"""

def getHonestUsers(n, rho): 
    """ 
    Computes the number of honest husers from the total n and rho
    (this simple computation is encapsulated in a function so that 
    the runding from floating point is done consistently all over the 
    code)
    
    Parameters:
    -----------
    n: int
        number of users
    rho: float
        proportion of honest users (see the Privacy section of the paper)
    
    Returns:
    --------
    nH: int 
        the number of honest users
    """ 
    return m.floor(n * rho) 
    
def c_squared_param(delta):  
    """ 
    It computs the  c^2 parameter of Gaussian Mecahnism (Appendix A 
    of in (Dwork et al., "The Algorithmic Foundations of Differential Privacy")
    
    Parameters:
    ------------
    delta: float
        delta parameter of differential privacy
        
    Returns:
    --------
    A float with c^2, the parameter of DP Appendix A of (Dwork et al.) 
    """
    return 2*mp.log(mp.fdiv(1.25,delta))


def c_squared_param_GOPA(delta, kappa, gtype):
    """
    Computes c^2 params as in Corollary 1 from delta, kappa and the 
    topology of G^H. Note that the value of c^2 in Gopa is slightly 
    different than its analogous for the Gaussian mechanism.  
    
    Parameters:
    -----------
    delta: float
        delta DP parameter in Corollary 1
    kappa: float
        kappa parameter of Corollary 1
    gtype: either GTYPE_CONNECTED, GTYPE_KOUT or GTYPE_COMPLETE (see constants above)
    
    Returns: 
    --------
    A float value with parameter c^2 of Corollary 1
    """ 
    
    assert(gtype == GTYPE_CONNECTED or gtype == GTYPE_KOUT or gtype==GTYPE_COMPLETE)
    a = a_KOUT if (gtype == GTYPE_KOUT) else a_NONKOUT 
    return 2*m.log(1.25) -  2*(m.log(1.25) + ((kappa+1)/kappa) * mp.log(delta/a))
    # ~ return 2*m.log(1.25) + 2*(m.log(1/1.25) + ((kappa+1)/kappa) * m.log(a/delta))
    

def noise_var_GM(eps, delta, sens):  
    """
    Computes the variance of the noise in trusted curator setting 
    of the Gaussian DP mechanism in Appendix A of 
    (Dwork et al., "The algorithmic foundations.. ")
    
    
    Parameters:
    -----------
    eps: float
        epsilon DP parameter
    delta: float
        delta DP parameter
    sens: float 
        sensitivity of the computation. Delta_2 f in Appendix A
    
    Returns: 
    --------
    A float value with the variance sigma^2 to obtain (eps, delta)-DP
    """ 
    return c_squared_param(delta) * ((sens/eps)**2)

def independent_noise_var_GOPA(nH, eps, delta, gtype, kappa):  
    """ 
    It calibrates the variance of individual independent noise (sigma_eta^2)
    of Gopa to obtain (eps, delta)-DP according to corollary 1
    
    Parameters:
    -----------
    nH: int
        number of honest users (n_H in Corollary 1)
    eps: float 
        epsilon DP parameter
    delta: float
        delta DP parameter
    gtype: either GTYPE_CONNECTED, GTYPE_KOUT or GTYPE_COMPLETE (see Corollary 1)
            type of communication graph
    kappa: float
        kappa parameter of Corollary 1 
   
    Returns: 
    -------- 
    A float value the independent noise variance sigma_eta^2 of Corollary 1
    """ 
    return c_squared_param_GOPA(delta, kappa, gtype) / (nH * (eps**2)) 
    

def independent_noise_var_GOPA_deltap(nH, eps, deltap):  
    """ 
    It computes the variance of individual (i.e. the noise per user) 
    independent noise (sigma_eta^2) of Gopa to obtain 
    (eps, delta)-DP, from delta prime of Corollary 1 
    
    Parameters:
    -----------
    nH: int
        number of honest users (n_H in Corollary 1)
    eps: float
        epsilon DP parameter
    deltap: float
        delta prime DP parameter 
    
    Returns: 
    -------- 
    the independent noise variance sigma_eta^2 according to Corollary 1,
    to obtain DP (assuming pairwise noise is also calibrated 
    accordingly)
    """ 
    return c_squared_param(deltap) / (nH * (eps**2))
   

def individual_noise_var_LocalDP(eps, delta): 
    """ 
    Computes the variance sigma^2 of the Gaussian Mechanism 
    to obtain (eps, delta)-DP in the Local DP setting
    
    Parameters:
    -----------
    nH: number of honest users (n_H in Corollary 1)
    eps: epsilon DP parameter
    deltap: delta prime DP parameter 
    
    Returns: 
    -------- 
    A float value that is the independent noise variance added by each
    user to its private value 
    """ 
    
    # This is just Gaussian Machanism with sensitivity set to 1
    return noise_var_GM(eps, delta, 1)

# Variance in the generation of pairwise noise 
def pairwise_noise_var_kout(rho, nH,  k, kappa, ind_noise_var): 
    """ 
    Computes the pairwise noise variance sigma_Delta^2 for Gopa in 
    Corollary 1 for k-out random graphs
     
    nH: is the number of honest users (n_H in Corollary 1)
    kappa:  kappa parameter of Corollary 1
    k: k  parameter for k out Graphs in Corollary 1 
    ind_noise_var: independent noise variance sigma_eta^2 of Crl 1
    rho: proportion of honest users of Crl 1
    
    Returns:
    ---------
    sigma_Delta^2: pairwise noise variance of Gopa to satisfy DP 
    """ 
    return kappa * ind_noise_var * nH * (1/(m.floor((k-1)*rho/3)-1) + (12+ 6*m.log(nH))/nH)  

def pairwise_noise_var_connected(nH, kappa, ind_noise_var): 
    """ 
    Computes the pairwise noise variance sigma_Delta^2 for Gopa in 
    Corollary 1 for arbitrarily connected graphs
     
    nH: is the number of honest users (n_H in Corollary 1)
    kappa:  kappa parameter of Corollary 1
    ind_noise_var: independent noise variance sigma_eta^2 of Crl 1
    
    Returns:
    ---------
    sigma_Delta^2: pairwise noise variance of Gopa to satisfy DP 
    """ 
    return kappa * ind_noise_var * (nH**2) / 3

def pairwise_noise_var_complete(kappa, ind_noise_var): 
    """ 
    Computes the pairwise noise variance sigma_Delta^2 for Gopa in 
    Corollary 1 for complete graphs
     
    kappa:  kappa parameter of Corollary 1
    ind_noise_var: independent noise variance sigma_eta^2 of Crl 1
    
    Returns:
    ---------
    sigma_Delta^2: pairwise noise variance of Gopa to satisfy DP 
    """  
    return kappa * ind_noise_var

def getDeltaFromDeltaP(gtype, deltap, kappa): 
    """
    Computes delta of Corollary 1 from delta prime 
      
    Parameters:
    ----------
    gtype:  type of communication graph. It can be either GTYPE_CONNECTED, 
            GTYPE_KOUT or GTYPE_COMPLETE
    deltap: delta prime parameter of Corollary 1
    kappa: kappa parameter of Corollary 1
    
    Returns:
    --------
    delta: delta parameter of Corollary 1
    """
   
    assert(gtype == GTYPE_CONNECTED or gtype == GTYPE_KOUT or gtype==GTYPE_COMPLETE)
    a = a_KOUT if (gtype == GTYPE_KOUT) else a_NONKOUT            
    return a * (deltap/1.25)**(kappa/(kappa+1))
   

def getDeltaPFromDelta(gtype, delta, kappa):
    """
    The inverse of getDeltaFromDeltaP (computes delta prime from delta)
    
    Parameters:
    -----------
    gtype: type of communication graph. It can be either GTYPE_CONNECTED, 
            GTYPE_KOUT or GTYPE_COMPLETE
    delta: delta parameter of Corollary 1
    kappa: kappa parameter of Corollary 1
    
    Returns:
    ------
    delta prime paramter in Corollary 1
    """
    
    assert(gtype == GTYPE_CONNECTED or gtype == GTYPE_KOUT or gtype==GTYPE_COMPLETE)
    a = a_KOUT if (gtype == GTYPE_KOUT) else a_NONKOUT 
    return 1.25 * (delta/a)**((kappa+1)/kappa)
    
    

def getKappaFromDeltaDeltaP_Thm1(delta, deltap): 
    """ 
    Returns kappa parameter, with a=1.25 as in Corollary 1 for 
    abitrarlily connected graphs or complete graphs. This can be 
    used for other types of graphs, exept for theoretical results of 
    k-out graphs, which require a=3.75 
    Parameters:
    -----------
    delta: delta satisfied in the DP guarantees
    deltap: auxiliary delta' of Corollary 1 used to calbrate noise
    
    Returns:
    -------
    kappa: parameter Corollary 1
    
    """ 
    kappa = m.log(1.25/delta) / m.log(delta/deltap) 
    return kappa
    
    
def getKappaFromDeltaDeltaP_Thm4(delta, deltap): 
    """ 
    Returns kappa parameter, with a=3.25 as in Corollary 1 
    k-out graphs.
    Parameters:
    -----------
    delta: delta satisfied in the DP guarantees
    deltap: auxiliary delta' of Corollary 1 used to calbrate noise
    
    Returns:
    -------
    kappa: parameter Corollary 1
    
    """ 
    kappa = m.log(3.75/delta) / m.log(delta/deltap) 
    return kappa
    
    

def get_admissible_k_kout(n, rho, delta):
    # @n: number of users
    # @rho: proportion of maliciuous users of K-out privacy Theorem 
    # @delta: delta parameter of K-out privacy Theorem 
    #
    # Returns @k, which is the minimum admissible value 
    # for a @k-out random graph to obain the DP guarantees in
    # the K-out privacy Theorem.  
 
    rho_n = rho * n
    delta2 = delta/3
    # check conditions of theorem 3
    assert(rho_n  > 81)  # check that we have chosen a valid rho
    bound1 = 4 * m.log(2 * rho_n  / (3 * delta2)) / rho
    print(bound1)
    bound2 = 6 * m.log(rho_n / 3) / rho
    print(bound2)
    bound3 = 3/(2*rho) + 9 * m.log(2 * m.e / delta2) / (4* rho) 
    print(bound3)
    k = max(bound1, bound2, bound3)
    return m.ceil(k) 
    


"""
----------------------------------------------------
Functions that compute  the total noise of protocols
---------------------------------------------------- 
"""


def total_noise_var_GOPA(n,nH, eps, delta, gtype, kappa):  
    """ 
    Computes the noise of the estimation of Gopa when executed with 
    n users, nH of them are honest, for the types of graph
    specified in Corollary 1, and in order to obtain (eps, delta)-DP. 
    
    Parameters:
    ----------
    n: int
        the number users
    nH: int 
        the number of honest Gopa users
    eps: float
        epsilon DP parameter
    delta: float
        delta DP parameter
    gtype: either GTYPE_KOUT or GTYPE_CONNECTED or GTYPE_COMPLETE
        topology of communication graph G (as described in Corollary 1)
    kappa: kappa parameter of Corollary 1

    Returns: 
    --------
    Returns a float value that is the total variance of the mechanism 
    """
    return independent_noise_var_GOPA(nH, eps, delta, gtype, kappa)/n

def total_noise_var_LocalDP(n, eps, delta):
    """ 
    Computes the noise of the estimation of private averaging 
    of LDP when executed with n users in order to obtain (eps, delta)-DP. 
    
    Parameters:
    n: int
        the number of users  
    eps: float
        the the epsilon DP parameter
    delta: float
        the delta DP parameter 
    
    Returns:
    ------- 
    Returns a float value that is the total variance of the mechanism
    """
    return individual_noise_var_LocalDP(eps,delta)/n
    

def total_noise_var_CentralDP(n, eps, delta): 
    """
    Computes the noise of the estimation of private averaging 
    of Central DP (with trusted curator) when executed with n users 
    in order to obtain (eps, delta)-DP.
    
    Parameters:
    n: int
        the number of users  
    eps: float
        the the epsilon DP parameter
    delta: float
        the delta DP parameter  
    
    Returns:
    --------
    A float value which is the total variance of the mechanism
    """ 
    return noise_var_GM(eps, delta, 1/n)
  
  
def total_noise_var_GOPA_dropouts_edges_peernoise_variance(n, gamma, harmful_edges_var, pairwise_var): 
    nD = m.ceil(n*gamma) 
    return  (pairwise_var/((n - nD)**2))**2 * harmful_edges_var  
    
def total_noise_var_GOPA_dropouts_edges_peernoise(n, gamma, harmful_edges, independent_var,  pairwise_var): 
    nD = m.ceil(n*gamma) 
    return (independent_var / (n - nD)) + (pairwise_var * harmful_edges)/((n - nD)**2)  
    
def total_noise_var_GOPA_dropouts_edges(n, k, gamma, rho, eps, delta,  kappa, harmful_edges): 
    nH = getHonestUsers(n, rho)
    ind_v = independent_noise_var_GOPA(nH, eps, delta, GTYPE_KOUT, kappa)
    pair_v = pairwise_noise_var_kout(rho, nH, k, kappa, ind_v)
    return total_noise_var_GOPA_dropouts_edges_peernoise(n, gamma, harmful_edges, ind_v,  pair_v)




def sample_dropouts(n, k, gamma):
    """
    Old function that simualtes a proportion @gamma of dropouts, but
    not used in any of the experiments. We use C code for this. 
    (Not used in plots)
    """ 
    nD = m.ceil(gamma * n) 
    neighbors = [ set() for _ in range(n) ] 
    for u in range(n): 
        neigh = 0  
        neighreq = set()
        while True: 
            neighreq.update(rnd.sample(range(n), k - neigh))
            neigh += len(neighreq.difference({u}.union(neighbors[u])))
            neighbors[u] = neighbors[u].union(neighreq)
            assert(neigh <= k) 
            if neigh == k:
                break  
        for v in neighbors[u]:
            neighbors[v].add(u)
    # ~ print(neighbors)
    dropouts = set()
    dropouts.update(rnd.sample(range(n), nD))
    harmful_edges = 0; 
    for i in dropouts:
        harmful_edges += len(neighbors[i].difference(dropouts)) 

    return harmful_edges
    

def worstcase_dropped_edges(t, n, k, gamma): 
    """ 
    Gets the worst case sampled edges in many trials @t
    (Not used in plots)
    """ 
    nD = m.ceil(gamma * n) 
    print("2* (n * gamma) * k: ", str(2 * nD * k))
    worst = 0 
    for i in range(t): 
        harmful_edges = sample_dropouts(n, k, gamma)
        if worst < harmful_edges: 
            worst = harmful_edges
    return worst
            
""" 
Functions for the simulation of an entire execution
""" 
def vectmean(X, n, p): 
    # compute the mean of the vectors
    X_sum = np.empty(p) 
    for u in range(n):
        X_sum += X[u]
    return X_sum/n
    
def perturbed_average(X, n,  variance,  seed): 
    rng = np.random.RandomState(seed)
    p = len(X[0])
    # compute the mean of the vectors
    X_avg = vectmean(X,n,p)
    # add noise of Gopa to the extimation 
    return X_avg + rng.normal(scale=m.sqrt(variance),size=p)

# Simulates the outcome of Gopa when every user has a private vector X[u] of private values an dimension d
# Gopa is executed with k-out random grapgs, and there are no dropouts
def outcome_GOPA_opt(X, n, k, rho, eps, delta, kappa, seed):
    nH = getHonestUsers(n, rho) 
    noise_var = total_noise_var_GOPA(n, nH, eps, delta, GTYPE_KOUT, kappa) 
    return perturbed_average(X, n,  noise_var, seed)
    
# Simulates the outcome of Central DP when every user has a private vector X[u] of private values an dimension d
def outcome_CentralDP(X, n, eps, delta, seed):
    noise_var = total_noise_var_CentralDP(n, eps, delta)
    return perturbed_average(X, n, noise_var, seed)
    
def outcome_LocalDP(X, n, eps, delta, seed): 
    return perturbed_average(X, n, total_noise_var_LocalDP(n, eps, delta), seed)
    

    

def final_var_dropouts_kappa_gamma(n, rho, eps, delta, kappa, gamma):
    assert(gamma <= rho) 
    nH = getHonestUsers(n, rho)
    k = get_admissible_k_kout(n, rho, delta)
    ind_var = independent_noise_var_GOPA(nH, eps, delta, GTYPE_KOUT, kappa)
    pair_var = pairwise_noise_var_kout(rho, nH, k, kappa, ind_var) 
    cutted_edges = 2 * k * (gamma * n)
    final_var = total_noise_var_GOPA_dropouts_edges_peernoise(n, gamma, cutted_edges, ind_var, pair_var)
    

def advancedComposition(epsp, deltapp, n_iter):      
    """
    Computes eps and delta values requrired in each 
    update such that if each step is (eps, delta)-DP,
    then the composition the n_iter updates 
    satisfies (epsp, deltapp)-DP. It applies advanced composition 
    in (Dwork and Roth, 2014). 
    
    Parameters:
    ----------
    epsp: float
        final epsilon desired for DP
    deltapp: float 
        final delta desired for DP
    @n_iter: int
        total number of updates
     
    Returns:
    ------
    The DP parameters that should satisfy each update
    eps: float
        epsilon DP parameter of an individual step
    delta: float 
        delta DP parameter of an individual step
    """ 
    delta = deltapp / (n_iter+1) 
    eps = epsp / (2 * m.sqrt(-2*n_iter*m.log(delta)))
    return eps, delta


