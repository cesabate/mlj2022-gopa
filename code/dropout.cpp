/* Compile with 
  g++ -O3 -fopenmp dropout.cpp -o dropout -lm
*/ 

#include <iostream>
#include <set>
#include <random>
#include <vector>
#include <algorithm>
#include <math.h> 
#include <assert.h>
#include <string> 

/**
 * Pick a subset of k elements efficiently 
 * from a set of N elements
 */ 
std::set<int> pick(int N, int k)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    
    std::set<int> elems;
    for (int r = N - k; r < N; ++r) {
        int v = std::uniform_int_distribution<>(1, r)(gen);

        // there are two cases.
        // v is not in candidates ==> add it
        // v is in candidates ==> well, r is definitely not, because
        // this is the first iteration in the loop that we could've
        // picked something that big.

        if (!elems.insert(v).second) {
            elems.insert(r);
        }   
    }
    return elems;
}


unsigned long test_once(int n, int k, double gamma) {
    std::set<int> neighbors[n];
    for (int i = 0; i < n; i++) 
        neighbors[i].empty(); 
    // generate graph
    for (int u = 0; u < n; u++) {
        int neigh = 0;
        int nsize = neighbors[u].size();
        do { 
            std::set<int> newneigh = pick(n, k-neigh);
            neighbors[u].insert(newneigh.begin(), newneigh.end());
            newneigh.erase(u);
            neigh += neighbors[u].size() - nsize; 
            nsize = neighbors[u].size();
        } while (neigh < k); 
        // insert current neighbor succesful request in the table of other neighbors
        for(std::set<int>::iterator it = neighbors[u].begin(); it != neighbors[u].end(); ++it) 
            neighbors[*it].insert(u);   
    }
    // generate dropouts and compute harmful edges
    int nD = ceil(double(n) * gamma); 
    std::set<int> dropouts = pick(n, nD);
    unsigned long harmful_edges = 0;  
    for (std::set<int>::iterator it = dropouts.begin(); it != dropouts.end(); ++it) {
        std::set<int> removed_edges; 
        removed_edges.empty(); 
        // the cutted edges are all edges of a dropped user minus edges that end in other dropout
        std::set_difference(neighbors[*it].begin(), neighbors[*it].end(), 
                                dropouts.begin(), dropouts.end(), 
                                std::inserter(removed_edges, 
                                removed_edges.end()));
        harmful_edges += (unsigned long)(removed_edges.size()); 
    }
    
    return harmful_edges; 
    //~ for (int u = 0; u < n; u++) {
        //~ std::cout << u << ": "; 
        //~ for(std::set<int>::iterator it = neighbors[u].begin(); it != neighbors[u].end(); ++it)
            //~ std::cout << *it << " "; 
        //~ std::cout << std::endl;
    //~ }
}

void test(int sample_size, int n, int k, double gamma) {

    int worst = 0; 
    std::cout << "samples=" << sample_size << ", n="<<n <<", k="<<k<<", gamma="<<gamma << std::flush; 
    //~ omp_lock_t writelock;
    //~ omp_init_lock(&writelock);
    
    double  avg_harmful = 0; 
    double avg_harmfulsq = 0; 
    //~ #pragma omp parallel for
    for (int i = 0; i  < sample_size; i++) {
        unsigned long harmful_edges = test_once(n, k, gamma); 
    //~ omp_set_lock(&writelock);
        if (worst < harmful_edges) 
            worst = harmful_edges; 
        avg_harmful += double(harmful_edges)/double(sample_size); 
        avg_harmfulsq += double(harmful_edges * harmful_edges)/double(sample_size); 

    //~ omp_unset_lock(&writelock);
    } 
    double var = avg_harmfulsq -  avg_harmful* avg_harmful ; 
    std::cout << " max harmful edges: " <<  worst << "  mean: " <<avg_harmful<< "  std: " << sqrt(var) << std::endl; 
}

int main(int argc, char* argv[])
{
    assert(argc >= 4);
    int n_samples = std::stoi(argv[1]);
    int n = std::stoi(argv[2]);
    int k = std::stoi(argv[3]);
    double gamma = std::stod(argv[4]);
    test(n_samples, n, k, gamma); 

    return 0; 
}
