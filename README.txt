Results Generated with:
----------------------
* Core i7-6600U
* Python version: 3.7.3
* G++ version:  7.5.0


Required Python Packages:
-------------------------
    - scikit-learn version 0.24.1
    - mpmath
    - matplotlib
	- scipy


Generate Figure 2(a):
--------------------
cd fig2a
python gen.py


Generate Figure 2(b):
---------------------
cd fig2b
python gen.py

Generate Figure 2(c):
---------------------

cd fig2c

# to run the complete experiment (estimated time: between 15-30 min) use
python gen.py t

# to plot precomputed SGD results, use 
python gen.py l 


Generate Figure 3:
------------------
cd fig3

# compile the dropout simulator
g++ -O3 ../code/dropout.cpp -o ../code/dropout -lm

# to run the complete simulation (time: 2 hours) use
python gen.py s

# to compute Figure 3 from precomputed simulations use 
python gen.py l



Generate values for Tables 2 and 3:
------------------------------------

cd tables2-3

# compile the random graph simulator
g++ ../code/rndexp.C -o ../code/rndexp

# to run the entire simulation (~60 hours if rndexp.C is sequentially run) use:
python gen.py s 

# to run a quick simulation (less conservative results, estimated time: 20 minutes) use:
python gen.py q

# note that the simulation above only generates 100 random graphs per setting 
# (in the paper, we use results over 10000 graphs)

# to compute values of Tables 2 and 3 from precomputed simulations use 
python gen.py l
