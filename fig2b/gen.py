# run with: python3 gen.py

import sys

sys.path.append("../code/")



import noisecalibration as nc
from matplotlib import pyplot as plt 
import sgd 
plt.rcParams.update({'font.size': 15})

""" 
Parameters of Corollary 1 and for privacy budget 
"""
n_workers=10000 # number of Gopa users
epsp=1 # epsilon privacy budget of the composition 
rho=0.5    # proportion of honest users 
nH = nc.getHonestUsers(n_workers, rho) # number of honest users
deltapp = 1/(nH**2)  # delta privacy parameter of the composition 
kappa=10 # kappa parameter of Corollary 1


"""
Optimal step size for 50 iterations 
""" 
gamma_cdp =  2.875 
gamma_gopa =  2.3  
gamma_ldp = 0.01 


n_iter= 50 # number of iterations of the model 

seed1=32
seed2=34
seed3=36

n_runs=1

# compute a model and get a snapshot of the gradient descent  
_ , _ , oll_cdp, ill_cdp , _ = sgd.testCDP(n_runs, n_iter, n_workers, gamma_cdp, epsp, deltapp,  seed1, False)

_, _, oll_gopa, ill_gopa , _ = sgd.testGopa(n_runs, n_iter, n_workers, gamma_gopa, epsp, deltapp, rho, kappa, seed2, False)

# ~ _, _, oll_ldp, ill_ldp, _ = sgd.testLDP(n_runs, n_iter, n_workers, gamma_ldp, epsp, deltapp,  seed3, False)


# plot the snapshots 

linestyle_cdp =        (0, (3, 1, 1, 1, 1, 1))
    
plt.xlabel("Number of iterations")
plt.ylabel("Objective function")
plt.plot(ill_gopa[0], oll_gopa[0], label=r'FedSGD with GOPA $(\rho=0.5)$', linewidth=3) 
plt.plot(ill_cdp[0], oll_cdp[0], linestyle=linestyle_cdp, linewidth=3, label=r'FedSGD with Trusted curator (Central DP)')
plt.legend() 
plt.show() 




