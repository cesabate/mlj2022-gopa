import re
import os
import subprocess
import sys
import math as m
sys.path.append("../code/")
import noisecalibration as nc
import utilitary as ut  
from matplotlib import pyplot as plt

allruns = [] # results of different sets of runs (runs are done in  different machines 
            # to paralellize the tests)
aggreg = [] 
    
# to store results and reuse them in Table 2
pair_varl = [] 
kl = []

failidx = 3
worstwidx = 6
            
def addnewresults(rglines): 
    """ 
    Add new results from the output of one machine into testll
    
    Parameters:
    rglines: lines of the output of runs in one machine
    """ 
    machrun = []  # runs in one machine
    for i in range(len(rglines)): 
        params = re.findall(r'(\d+(?:\.\d+)?)', rglines[i].decode('utf-8'))
        machrun.append(params)
    allruns.append(machrun) 
    
    
    
    
def postprocess(): 
    """ 
    Post process results:
    """  
    
    for i in range(len(allruns[0])):
        if (len(allruns[0][i]) > 0):
            aggreg.append([])
            aggreg[i] += allruns[0][i] 
    
    
    for machrun in allruns[1:]:
        for lineidx in range(len(machrun)): 
            aggreg[lineidx][failidx] = float(aggreg[lineidx][failidx]) + float(machrun[lineidx][failidx]) #sum the number of fails
            aggreg[lineidx][worstwidx] = max(float(aggreg[lineidx][worstwidx]), float(machrun[lineidx][worstwidx]))    
            
             
def helpmsg():
    print("usage: python gen.py mode")
    print("modes:") 
    print("\t l: load stored simulation results and generate safe values of sigma_eta")
    print("\t s: simulate random graphs and generate safe values of sigma_eta")
    print("\t q: quickly simulate random graphs and generate safe values of sigma_eta")
    exit()
                
if (len(sys.argv) != 2): 
    helpmsg()
    exit()
    

if (sys.argv[1] == 's'): 
    for i in range(1,11):
        with open('results1.'+str(i), 'w') as hdl:
            subprocess.call(["../code/rndexp", "10000", str(i)], stdout=hdl)
        
        with open('results1.'+str(i), 'rb', buffering=True) as hdl: 
            rglines = hdl.readlines()
            addnewresults(rglines)

elif (sys.argv[1] == 'l'):
    # ~ with open('rndexp.out', 'rb', buffering=True) as f: 
    for i in range(1,11): 
        with open('results.'+str(i), 'rb', buffering=True) as f: 
            rglines  = f.readlines()
            addnewresults(rglines) 
        
elif (sys.argv[1] == 'q'): 
    with open('results.q', 'w') as hdl:
            subprocess.call(["../code/rndexp", "100", "12"], stdout=hdl)
        
    with open('results.q', 'rb', buffering=True) as hdl: 
            rglines = hdl.readlines()
    addnewresults(rglines)
            
    
else: 
    helpmsg()
    exit()


# postprocess results in aggreg 
postprocess()

print("\nTable 3:") 
for params in aggreg: # for each expermient do:
    # parse parameters 
    #params = re.findall(r'(\d+(?:\.\d+)?)', rgl.decode('utf-8'))
    fail = float(params[3])
    if (fail > 0): # if parameters failed once, ignore the experiment
        continue
    
    n = int(params[0])
    nH = int(params[1])
    k = int(params[2])
    kl.append(k)
    worstw = float(params[6])
    
    # set other parameters
    rho = nH/n
    eps = 0.1
    deltap = 1/(nH **2)
    delta = 10 * deltap 
    kappa = nc.getKappaFromDeltaDeltaP_Thm1(delta, deltap) 
    # calibrate noise
    ind_var = nc.independent_noise_var_GOPA_deltap(nH, eps, deltap)
    pair_var = kappa * ind_var * nH * worstw
    
    pair_varl.append(pair_var)
    
    print("n=", n, "rho=", rho, "k=", k ,"Standard Deviation of Pairwise Noise (sigma_delta) =", m.sqrt(pair_var))
    

print("\nTable 2:") 


n = 10000
eps = 0.1

nH1 = 10000
k1 = 105
deltap1 = 1/(nH1**2) 
delta1 = 10 * deltap1
kappa1 = nc.getKappaFromDeltaDeltaP_Thm1(delta1, deltap1)
kappa1_kout = nc.getKappaFromDeltaDeltaP_Thm4(delta1, deltap1)
ind_var1 = nc.independent_noise_var_GOPA_deltap(nH1, eps, deltap1)
row1 = 9 # row of the run within the experiment 


nH2 = 5000
k2 = 203
deltap2 = 1/(nH2**2) 
delta2 = 10 * deltap2
kappa2 = nc.getKappaFromDeltaDeltaP_Thm1(delta2, deltap2)
kappa2_kout = nc.getKappaFromDeltaDeltaP_Thm4(delta2, deltap2)
ind_var2 = nc.independent_noise_var_GOPA_deltap(nH2, eps, deltap2)
row2 = 11 # row of the run within the experiment 

print("Type of graph \t\t\t\t rho = 1 \t\t\t\trho = 0.5")

print("Complete Graph: \t\t\t", m.sqrt(nc.pairwise_noise_var_complete(kappa1, ind_var1)), 
                    ",\t\t", m.sqrt(nc.pairwise_noise_var_complete(kappa2, ind_var2)))
                    
print("k-out Graph (Theorem 4): \t\t", m.sqrt(nc.pairwise_noise_var_kout(nH1/n, nH1, k1, kappa1_kout, ind_var1)), 
                                    "(k=", k1,")\t", m.sqrt(nc.pairwise_noise_var_kout(nH2/n, nH2, k2, kappa2_kout, ind_var2)),"(k=", k2,")")

print("k-out Graph (Numerical Simulation): \t", m.sqrt(pair_varl[row1]), "(k=", kl[row1] , ")", 
                                    ",\t", m.sqrt(pair_varl[row2]),"(k=", kl[row2] , ")")
                                    
print("Worst-case: \t\t\t\t", m.sqrt(nc.pairwise_noise_var_connected(nH1, kappa1, ind_var1)), 
                                    ",\t\t", m.sqrt(nc.pairwise_noise_var_connected( nH2, kappa2, ind_var2)))




    
