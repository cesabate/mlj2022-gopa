# - run with: python gen.py
# - note that gamma here is the proportion of non-rolled back noise terms  

import sys
import os
import math as m
import pickle 
from matplotlib import pyplot as plt


sys.path.append("../code/")
import noisecalibration as nc 
import utilitary as ut

n_runs = 100
n_nodes = 10000
rho = 0.5
eps = 0.1
kappa = 0.3
k = 203 
nH = nc.getHonestUsers(n_nodes, rho) 
delta = 10/(nH**2)

# check that k is admissible according to our DP results
assert(k >= nc.get_admissible_k_kout(n_nodes, rho, delta)) 

class PlotData: 
    def __init__(self, gammal):
        self.gammal = gammal 
        self.meanl = [] # for each gamma value, the mean amount of harmful edges over all runs 
        self.stdl = []  #  for each gamma value, the standard deviation of the amount of harmful edges over all runs 


# gamma here is not the learning rate but the proportion of "catastrophic dropouts" 
gammal = [0, 0.0005, 0.005, 0.01, 0.015, 0.02, 0.025, 0.03, 0.035, 0.04]  # list of values of gamma to try
seed = hash(sum(gammal))

if (len(sys.argv) != 2): 
    print("usage: python gen.py mode")
    print("modes:") 
    print("\t l: load stored results and plot them")
    print("\t s: simulate dropouts and plot impact on utility")
    exit()


if (sys.argv[1] == 'l'):
    with open('PlotData_'+str(seed)+'.pickle', 'rb') as handle:
        pltd = pickle.load(handle)
        
elif (sys.argv[1] == 's'):

    pltd  = PlotData(gammal) 

    for i in range(len(pltd.gammal)): 
        print("Testing dropouts: proportion of catastrophic dropouts=", pltd.gammal[i], "n. tests=", n_runs)
        out = os.popen("../code/dropout "+ str(n_runs) +" "+ str(n_nodes) + " "+ str(k) + " "+ str(pltd.gammal[i]))
        _ , _ , _, _ , _, mean, std =  ut.scan_input('%d %d %d %f %d %f %f', out)
        print("Avg n. of non cancelled pairwise noise terms=", mean, "std=", std)
        print(" ")
        pltd.meanl.append(mean) 
        pltd.stdl.append(std)
    
    # store data
    # ~ with open('PlotData_'+str(seed)+'.pickle', 'wb') as handle:
        # ~ pickle.dump(pltd, handle, protocol=pickle.HIGHEST_PROTOCOL)
  
else: 
    print("usage: python gen.py mode")
    print("modes:") 
    print("\t l: load stored results and plot them")
    print("\t s: simulate dropouts and plot impact on utility")
    exit()
    
   
# compute compute the impact on the variance based on the avg amount of non-cancelled edges  
ind_var = nc.independent_noise_var_GOPA(nH, eps, delta, nc.GTYPE_KOUT, kappa)
pair_var = nc.pairwise_noise_var_kout(rho, nH, k, kappa, ind_var) 
gopa_varl = [ nc.total_noise_var_GOPA_dropouts_edges_peernoise(n_nodes, pltd.gammal[i],pltd.meanl[i] , ind_var, pair_var) for i  in range(len(pltd.gammal))  ]
gopa_varl_std = [ m.sqrt(nc.total_noise_var_GOPA_dropouts_edges_peernoise_variance(n_nodes, pltd.gammal[i], (pltd.stdl[i])**2,  pair_var)) for i  in range(len(pltd.gammal)) ]
noise_cdp = nc.total_noise_var_CentralDP(n_nodes, eps, delta) 
noise_ldp = nc.total_noise_var_LocalDP(n_nodes, eps, delta) 

nDv = [ m.ceil(gamma * n_nodes) for gamma in pltd.gammal ] 

linestyle_ldp =        (0, (3, 5, 1, 5, 1, 5))
linestyle_cdp =        (0, (3, 1, 1, 1, 1, 1))

plt.rc('font', size=16)
## plot values 
plt.ylabel("Variance of the estimated average")
plt.xlabel("Amount of non-rolled back dropouts")
plt.plot(nDv, gopa_varl, '-x', label=r'GOPA, $\rho='+str(rho)+'$, $k$-out graph with $k$='+str(k), color='tab:blue' )
plt.axhline(noise_cdp, linestyle=linestyle_cdp, label=r'Central DP with $n$ users', color="tab:orange")
plt.axhline(noise_ldp, linestyle=linestyle_ldp, label=r'Local DP with $n$ users' ,color="tab:green")
plt.legend()
plt.show()
    
    
